﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http;
using System.Web.Http.Cors;
using log4net;
using ShipIt.Models;
using ShipIt.Services;

namespace ShipIt.Controllers
{
    [EnableCors("*", "*", "*")]
    public class LikeApiController : ApiController
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(LikeApiController)); 

        private readonly CommentService _commentService = new CommentService();
        private readonly UserService _userService = new UserService();

        /// <summary>
        /// GET: api/LikeApi/
        /// Returns all the fans for a given comment
        /// </summary>
        /// <param name="pseudoGuid">Links response to original request. To be removed</param>
        /// <param name="commentId">The comment identifier</param>
        /// <param name="likerId">To be removed</param>
        /// <returns></returns>
        [HttpGet]
        public GetFansResponse GetFans([FromUri]GetFansRequest request)
        {
            var response = new GetFansResponse();
            var fans = RetrieveFans(request.MessageId);
            response.Fans = fans.ToArray();
            response.CurrentLikeCount = response.Fans.Length;
            response.CanLike = response.Fans.All(fan => fan.UserName != request.LikerId);
            response.PseudoGuid = request.PseudoGuid;
            log.InfoFormat("Got {0} fans", response.CurrentLikeCount);
            return response;
        }
        
        /// <summary>
        /// POST: api/LikeApi
        /// Likes a comment
        /// </summary>
        /// <param name="likeRequest"></param>
        /// <returns>A list of fans who like this comment</returns>
        [HttpPost]
        public LikeResponse Like(LikeRequest likeRequest)
        {
            var retVal = new LikeResponse();
            try
            {
                var like = new Like
                {
                    AuthorId = likeRequest.MessageAuthorId,
                    AuthorName = likeRequest.MessageAuthorName,
                    CommentId = likeRequest.MessageId,
                    FanId =  likeRequest.LikerId,
                    FanName = likeRequest.FanName,
                    Content = likeRequest.Message,
                    Url = likeRequest.ReviewUrl,
                    CommentDate = likeRequest.CommentDate.Substring(0, 10)
                };

                retVal.CurrentLikeCount = _commentService.Like(like);
                retVal.PseudoGuid = likeRequest.PseudoGuid;
                var fans = RetrieveFans(like.CommentId);

                retVal.Fans = fans.ToArray();

                var slackUsers =_userService.GetSlackUserList();

                var authorSlackUser = _userService.FindSlackUser(likeRequest.AuthorEmail, slackUsers);
                if (authorSlackUser != null)
                {
                    _userService.SendSlackMessage(authorSlackUser, likeRequest.FanName, like.Url);
                }


                return retVal;
            }
            catch (Exception exc)
            {
                Console.WriteLine(exc.ToString());
                return retVal;
            }
        }

        [HttpPost]
        public UnlikeResponse Unlike(UnlikeRequest request)
        {
            var response = new UnlikeResponse();
            response.CurrentLikeCount = _commentService.Unlike(request.MessageId, request.FanId);
            response.PseudoGuid = request.PseudoGuid;
            var fans = RetrieveFans(request.MessageId);
            response.Fans = fans.ToArray();
            return response;
        }

        private IEnumerable<UserDTO> RetrieveFans(string commentId)
        {
            var likes = _commentService.GetLikes(commentId);
            var fans = likes
                .Select(
                    l => new UserDTO
                    {
                        UserName = l.FanId,
                        Name = l.FanName,
                    });
            return fans;
        }
    }
}
