﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using PagedList;
using ShipIt.Models;
using ShipIt.Services;

namespace ShipIt.Controllers
{
    public class TopCommentsController : Controller
    {
        private const int DaysSinceLastWeek = -7;
        private const int LastMonth = -1;
        private const int LastSixMonths = -6;
        private const int MinimumLikesNumberToEnterTop = 1;
        private const int DefaultStartPage = 1;
        private const int DefaultPageSize = 8;

        private readonly CommentService _commentService = new CommentService();

        public ActionResult Details(string commentId, int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            ViewBag.ItemsPerPage = DefaultPageSize;

            var comment = _commentService.GetCommentById(commentId);
            var likes = _commentService.GetLikes(commentId).ToPagedList(page, pageSize);
            var selectedCommentDetailsViewModel = new VotedCommentDetailsModel
            {
                CommentId = comment.CommentId,
                CommentContent = comment.Content,
                CommentUrl = comment.Url,
                CommentAuthor = new GerritUserModel()
                    {
                        Name  = comment.AuthorName,
                    },
                Votes = likes,
            };


            return PartialView("_TopCommentDetails", selectedCommentDetailsViewModel);
        }

        public ActionResult Index(int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            ViewBag.ItemsPerPage = DefaultPageSize;

            var lastMonthFilter = DateTime.Now.AddMonths(LastMonth);
            var lastMonthCommentsViewModel = ConstructTimestampFilteredViewModel(lastMonthFilter, page, pageSize);

            return View("~/Views/TopComments/TopComments.cshtml", lastMonthCommentsViewModel);
        }

        public ActionResult IndexAsync(int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            var isAjax = Request.IsAjaxRequest();
            ViewBag.ItemsPerPage = DefaultPageSize;

            var lastMonthFilter = DateTime.Now.AddMonths(LastMonth);
            var lastMonthCommentsViewModel = ConstructTimestampFilteredViewModel(lastMonthFilter, page, pageSize);

            return isAjax
                ? (ActionResult)PartialView("_TopCommentsTable", lastMonthCommentsViewModel)
                : View("~/Views/TopComments/TopComments.cshtml", lastMonthCommentsViewModel);
        }

        public JsonResult FilterByQueryStringJson(string queryString)
        {
            return Json(ConstructQueryFilteredViewModel(queryString), JsonRequestBehavior.AllowGet);
        }

        public ActionResult FilterByQueryString(string queryString)
        {
            return PartialView("_TopCommentsTable", ConstructQueryFilteredViewModel(queryString));
        }

        public ActionResult FilterByAuthor(string queryString, int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            var filteredComments = _commentService.GetAllFilteredByAuthorName(queryString);
            var viewModelList = ToPagedCommentsList(filteredComments, page, pageSize);

            return PartialView("_TopCommentsTable", viewModelList);
        }

        public ActionResult FilterByComment(string queryString, int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            var filteredComments = _commentService.GetAllFilteredByCommentContent(queryString);
            var viewModelList = ToPagedCommentsList(filteredComments, page, pageSize);

            return PartialView("_TopCommentsTable", viewModelList);
        }

        public ActionResult FilterByLikes(string queryString, int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            var filteredComments = _commentService.GetAllFilteredByLikesNumber(queryString);
            var viewModelList = ToPagedCommentsList(filteredComments, page, pageSize);

            return PartialView("_TopCommentsTable", viewModelList);
        }

        public ActionResult FilterByAllTime()
        {
            return PartialView("_TopCommentsTable", ConstructQueryFilteredViewModel());
        }

        public ActionResult FilterByLastWeek()
        {
            var lastWeekFilter = DateTime.Now.AddDays(DaysSinceLastWeek);
            return PartialView("_TopCommentsTable", ConstructTimestampFilteredViewModel(lastWeekFilter));
        }

        public ActionResult FilterByLastMonth()
        {
            var lastMonthFilter = DateTime.Now.AddMonths(LastMonth);
            return PartialView("_TopCommentsTable", ConstructTimestampFilteredViewModel(lastMonthFilter));
        }

        public ActionResult FilterByLastSixMonths()
        {
            var lastSixMonthsFilter = DateTime.Now.AddMonths(LastSixMonths);
            return PartialView("_TopCommentsTable", ConstructTimestampFilteredViewModel(lastSixMonthsFilter));
        }

        private IPagedList<VotedCommentModel> ConstructTimestampFilteredViewModel(DateTime timeStampFilter, int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            var filteredComments = _commentService.
                GetAllFilteredByLikesNumber(MinimumLikesNumberToEnterTop)
                                                  .Where(
                                                      comm =>
                                                          {
                                                              var lastOrDefault = _commentService.GetLikes(comm.CommentId).LastOrDefault();
                                                              return lastOrDefault != null && lastOrDefault.Timestamp.CompareTo(timeStampFilter) >= 0;
                                                          });

            return ToPagedCommentsList(filteredComments, page, pageSize);
        }

        private IPagedList<VotedCommentModel> ConstructQueryFilteredViewModel(string queryString = null, int page = DefaultStartPage, int pageSize = DefaultPageSize)
        {
            var allTimeComments = _commentService.
                GetAllFilteredByLikesNumberAndQueryString(MinimumLikesNumberToEnterTop, queryString);

            return ToPagedCommentsList(allTimeComments, page, pageSize);
        }

        private static IPagedList<VotedCommentModel> ToPagedCommentsList(IEnumerable<Comment> comments, int page, int pageSize)
        {
            return comments.Select(comment => new VotedCommentModel
            {
                CommentId = comment.CommentId,
                CommentContent = comment.Content,
                CommentDate = comment.CommentDate,
                CommentLikesCount = comment.LikesCounter,
                CommentUrl = comment.Url,
                AuthorName = comment.AuthorName
            }).ToPagedList(page, pageSize);
        }

    }
}
