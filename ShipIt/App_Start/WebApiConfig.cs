﻿using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Routing;

namespace ShipIt
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var cors = new EnableCorsAttribute("*", "*", "*");
            config.EnableCors(cors);

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "ApiByActionGet",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { controller = "LikeApi", action = "GetFans" },
                constraints: new { httpMethod = new HttpMethodConstraint(HttpMethod.Get) });
            config.Routes.MapHttpRoute(
                name: "ApiByActionPost",
                routeTemplate: "api/{controller}/{action}",
                defaults: new { controller = "LikeApi", action = "Like" }
            );
        }
    }
}
