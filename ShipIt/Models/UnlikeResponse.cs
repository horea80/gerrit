﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShipIt.Models
{
    public class UnlikeResponse
    {
        public string PseudoGuid { get; set; }
        public int CurrentLikeCount { get; set; }
        public UserDTO[] Fans { get; set; }
    }
}