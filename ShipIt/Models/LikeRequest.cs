﻿namespace ShipIt.Models
{
    public class LikeRequest
    {
        public string PseudoGuid { get; set; }

        public string MessageAuthorId { get; set; }

        public string MessageAuthorName { get; set; }

        public string MessageId { get; set; }

        public string Message { get; set; }
        
        public string LikerId { get; set; }
        
        public string ReviewUrl { get; set; }

        public string CommentDate { get; set; }

        public string FanName { get; set; }

        public string AuthorEmail { get; set; }
    }
}