﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShipIt.Models
{
    public class GetFansRequest
    {
        public string PseudoGuid { get; set; }
        public string MessageId { get; set; }
        public string LikerId { get; set; }
    }
}