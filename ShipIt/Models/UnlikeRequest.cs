﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShipIt.Models
{
    public class UnlikeRequest
    {
        public string PseudoGuid { get; set; }

        public string MessageId { get; set; }

        public string FanId { get; set; }

    }
}