﻿using System;

namespace ShipIt.Models
{
    public class VotedCommentModel
    {
        public string CommentId { get; set; }
        public string CommentDate { get; set; }
        public int CommentLikesCount { get; set; }
        public string CommentContent { get; set; }
        public string CommentUrl { get; set; }
        public string AuthorName { get; set; }

    }
}