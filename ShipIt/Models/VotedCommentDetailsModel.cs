﻿using System.ComponentModel;
using ShipIt.Services;
using PagedList;

namespace ShipIt.Models
{
    public class VotedCommentDetailsModel
    {
        [DisplayName("Comment Id")]
        public string CommentId { get; set; }

        [DisplayName("Comment Gerrit URL")]
        public string CommentUrl { get; set; }

        [DisplayName("Insight")]
        public string CommentContent { get; set; }

        public IPagedList<Like> Votes { get; set; }

        [DisplayName("Gerrit Hero")]
        public GerritUserModel CommentAuthor { get; set; }
    }
}