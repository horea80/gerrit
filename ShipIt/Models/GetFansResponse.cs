﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ShipIt.Models
{
    public class GetFansResponse
    {
        public string PseudoGuid { get; set; }
        public bool CanLike { get; set; }
        /// <summary>
        /// Redundant since one can calculate this from the number of users.
        /// To be removed
        /// </summary>
        public int CurrentLikeCount { get; set; }

        public UserDTO[] Fans { get; set; }
    }
}