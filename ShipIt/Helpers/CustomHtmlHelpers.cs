﻿using System;
using System.Web.Mvc;
namespace ShipIt.Helpers
{
    public static class CustomHtmlHelpers
    {
        private const int MaximumAllowedStringLengthInChars = 50;

        public static MvcHtmlString TextResizerHtmlHelper(this HtmlHelper htmlHelper, string source, int customMaxAllowedNumber = MaximumAllowedStringLengthInChars)
        {
            var sourceStringLength = source.Length;
            return sourceStringLength > customMaxAllowedNumber
                       ? new MvcHtmlString(source.Substring(0, Math.Min(sourceStringLength, customMaxAllowedNumber)) + "...")
                       : new MvcHtmlString(source);
        }

    }
}