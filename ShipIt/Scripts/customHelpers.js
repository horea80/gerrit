﻿var currentHash = "";

$(window).on('hashchange', function () {
    currentHash = location.hash;
    console.log(currentHash);
    
    switch (currentHash) {
        case "#allTime":
            $('#comments_table_section').load('/TopComments/FilterByAllTime');
            break;
        case "#lastWeek":
            $('#comments_table_section').load('/TopComments/FilterByLastWeek');
            break;
        case "#lastMonth":
            $('#comments_table_section').load('/TopComments/FilterByLastMonth');
            break;
        case "#lastSixMonths":
            $('#comments_table_section').load('/TopComments/FilterByLastSixMonths');
            break;
        default:
            break;
    }
    
});

if (jQuery) (function ($) {

    $.extend($.fn, {
        dropdown: function (method, data) {

            switch (method) {
                case 'show':
                    show(null, $(this));
                    return $(this);
                case 'hide':
                    hide();
                    return $(this);
                case 'attach':
                    return $(this).attr('data-dropdown', data);
                case 'detach':
                    hide();
                    return $(this).removeAttr('data-dropdown');
                case 'disable':
                    return $(this).addClass('dropdown-disabled');
                case 'enable':
                    hide();
                    return $(this).removeClass('dropdown-disabled');
            }

        }
    });

    function show(event, object) {
        
        var trigger = event ? $(this) : object,
			dropdown = $(trigger.attr('data-dropdown')),
			isOpen = trigger.hasClass('dropdown-open');

        // In some cases we don't want to show it
        if (event) {
            if ($(event.target).hasClass('dropdown-ignore')) return;

            event.preventDefault();
            event.stopPropagation();
        } else {
            if (trigger !== object.target && $(object.target).hasClass('dropdown-ignore')) return;
        }
        hide();

        if (isOpen || trigger.hasClass('dropdown-disabled')) return;

        // Show it
        trigger.addClass('dropdown-open');
        dropdown
			.data('dropdown-trigger', trigger)
			.show();

        // Position it
        position();

        // Trigger the show callback
        dropdown
			.trigger('show', {
			    dropdown: dropdown,
			    trigger: trigger
			});

    }

    function hide(event) {

        // In some cases we don't hide them
        var targetGroup = event ? $(event.target).parents().addBack() : null;

        // Are we clicking anywhere in a dropdown?
        if (targetGroup && targetGroup.is('.dropdown')) {
            // Is it a dropdown menu?
            if (targetGroup.is('.dropdown-menu')) {
                // Did we click on an option? If so close it.
                if (!targetGroup.is('A')) return;
            } else {
                // Nope, it's a panel. Leave it open.
                return;
            }
        }

        // Hide any dropdown that may be showing
        $(document).find('.dropdown:visible').each(function () {
            var dropdown = $(this);
            dropdown
				.hide()
				.removeData('dropdown-trigger')
				.trigger('hide', { dropdown: dropdown });
        });

        // Remove all dropdown-open classes
        $(document).find('.dropdown-open').removeClass('dropdown-open');

    }

    function position() {

        var dropdown = $('.dropdown:visible').eq(0),
			trigger = dropdown.data('dropdown-trigger'),
			hOffset = trigger ? parseInt(trigger.attr('data-horizontal-offset') || 0, 10) : null,
			vOffset = trigger ? parseInt(trigger.attr('data-vertical-offset') || 0, 10) : null;

        if (dropdown.length === 0 || !trigger) return;

        // Position the dropdown relative-to-parent...
        if (dropdown.hasClass('dropdown-relative')) {
            dropdown.css({
                left: dropdown.hasClass('dropdown-anchor-right') ?
					trigger.position().left - (dropdown.outerWidth(true) - trigger.outerWidth(true)) - parseInt(trigger.css('margin-right'), 10) + hOffset :
					trigger.position().left + parseInt(trigger.css('margin-left'), 10) + hOffset,
                top: trigger.position().top + trigger.outerHeight(true) - parseInt(trigger.css('margin-top'), 10) + vOffset
            });
        } else {
            // ...or relative to document
            dropdown.css({
                left: dropdown.hasClass('dropdown-anchor-right') ?
					trigger.offset().left - (dropdown.outerWidth() - trigger.outerWidth()) + hOffset : trigger.offset().left + hOffset,
                top: trigger.offset().top + trigger.outerHeight() + vOffset
            });
        }
    }

    $(document).on('click.dropdown', '[data-dropdown]', show);
    $(document).on('click.dropdown', hide);
    $(window).on('resize', position);

})(jQuery);


$(document).ready(
    function () {

        $('#comments_filter').on('click', '.comments_filter_list_elem', function () {

            var currentHeader = $('#comments_filter a').first();
            var removed = currentHeader.html().replace(/\(.*\)/g, '');
            currentHeader.html(removed);
            currentHeader.append('(' + $(this).html() + ')');
        });
        
        $('#header_container').on('click', '#help_btn_link', function () {

            console.log('Help button clicked');
            $('#help_install_extension_section').modal('show');

        });

        $('#help_modal').removeClass('fade');
        $('#help_modal').modal('hide');
        
    }
);

$('body').on('click', '.button_link', function () {

    console.log('Details button clicked');
    var url = $('#comments_details_section').data('url');

    var sd = $(this).attr('commentId');
    url += "?commentId=" + sd;

    $.get(url, function (data) {
        $('#details_data_container').html(data);

        $('#comments_details_section').modal('show');
    });
    
    $('#details_modal').removeClass('fade');
    $('#details_modal').modal('hide');
    
});

$('#comments_table_section').on('click', null, function () {
    
    $('#comments_table').filterable(

        {
            onlyColumns: [2, 3, 4],
        
            afterFilter:
                function (columnId, queryString) {
                    console.log("Filtered applied on column #" + columnId);

                    switch (columnId.toString()) {
                        case '2':
                            console.log("Filtering by author...");
                            filterByAuthorName(queryString);
                            break;
                        case '3':
                            console.log("Filtering by comments...");
                            filterByComments(queryString);
                            break;
                        default:
                            console.log("Filtering by likes count...");
                            filterByLikesCount(queryString);
                    }
                }
        }
    
    );
});

function filterByAuthorName(queryString) {
    var url = window.location.origin + '/TopComments/FilterByAuthor?queryString=' + queryString;
    $.get(url, function (data) {
        $('#comments_table_section').html(data);
    });
}

function filterByComments(queryString) {
    var url = window.location.origin + '/TopComments/FilterByComment?queryString=' + queryString;
    $.get(url, function (data) {
        $('#comments_table_section').html(data);
    });
}

function filterByLikesCount(queryString) {
    var url = window.location.origin + '/TopComments/FilterByLikes?queryString=' + queryString;
    $.get(url, function (data) {
        $('#comments_table_section').html(data);
    });
}