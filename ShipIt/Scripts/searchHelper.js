﻿$(document).on('submit', '#navbar_search_form', function (e) {
    e.preventDefault();
    var queryString = $('#navbar_search_form').serialize();
    postAjaxForm(e.target, queryString);
});

var postAjaxForm = function(form, queryString) {
    var updateElement = $('#comments_table_section');

    if (!$(updateElement).length) {
        alert('Unable to find update element');
        return false;
    }

    var url = form.action + '?' + queryString;

    $.get(url, function(data) {
        console.log(data);
        updateElement.html(data);
    });
    
    return false;
};


//setup before functions
var typingTimer;                //timer identifier
var doneTypingInterval = 1600;  //time in ms, 5 second for example
var textboxInput = '#navbar_search_form input';

//on keyup, start the countdown
$(textboxInput).keyup(function () {
    clearTimeout(typingTimer);
    typingTimer = setTimeout(doneTyping, doneTypingInterval);
});

//on keydown, clear the countdown 
$(textboxInput).keydown(function () {
    clearTimeout(typingTimer);
});

//user is "finished typing," do something
function doneTyping() {

    var url = window.location.origin + "/TopComments/FilterByQueryStringJson";

    //do automcomplete
    $(textboxInput).autocomplete({
        source: function (request, response) {
            $.ajax({
                async: false,
                cache: true,
                contentType: 'application/json; charset=utf-8',
                type: 'GET',
                url: url,
                data: { "queryString": request.term },
                success: function (data) {
                    response($.map(data, function (item) {
                        //console.log(item);
                        //console.log(request.term);
                        var queryString = request.term.toLowerCase();
                        if (item.AuthorName.toLowerCase().indexOf(queryString) >= 0) {
                            return { label: item.AuthorName, value: item.AuthorName };
                        }
                        else {
                            return { label: trimContent(item.CommentContent, true), value: trimContent(item.CommentContent, false) };
                        }
                    }));
                }
            });
        }
        //source: ['aaa', 'bbb', 'ccc']
    });

}

var maxNoOfChars = 15;

function trimContent(content, withSuspensePoints) {
    
    if (withSuspensePoints) {
        if (content.length > maxNoOfChars) {
            return content.substring(0, maxNoOfChars - 1) + "...";
        }
    } else {
        if (content.length > maxNoOfChars) {
            return content.substring(0, maxNoOfChars - 1);
        }
    }
    

    return content;
}



