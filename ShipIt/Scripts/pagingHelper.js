﻿$(document).ready(function () {
    
    $('.center').on('click', 'a', function(event) {
        console.log(this.href);
        event.preventDefault();
        $.ajax({
            type: 'GET',
            url: this.href,
            success: function(response) {
                console.log('Success retrieving comments...');

                $('#comments_table_section').html(response);
            },
            error: function(xhr, stats, errorMessage) {
                alert('Error loading page! Status: ' + stats + " (err: " + errorMessage + ")");
            }
        });

    });
    

});