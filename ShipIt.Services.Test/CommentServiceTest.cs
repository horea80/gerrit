﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Transactions;
using NUnit.Framework;

namespace ShipIt.Services.Test
{
    [TestFixture]
    public class CommentServiceTest
    {
        private TransactionScope _tx;

        [Test, Category("Integration")]
        public void LikeHappyFlow()
        {   
            var service = new CommentService();
            var like = new Like {AuthorId = "5", CommentId = "5", FanId = "5", Content = String.Empty, FanName = String.Empty, AuthorName = String.Empty, Url = String.Empty};
            service.Like(like);
            Assert.AreEqual(1, service.GetAllLikes().Count());
        }

        [Test, Category("Integration")]
        public void UnlikeHappyFlow()
        {
            var service = new CommentService();
            var like = new Like { AuthorId = "5", CommentId = "5", FanId = "5", Content = String.Empty, FanName = String.Empty, AuthorName = String.Empty, Url = String.Empty };
            service.Like(like);
            service.Unlike("5", "5");
            Assert.AreEqual(0, service.GetAllLikes().Count());
        }
        /// <summary>
        /// Forcing the creation of the database here with a simple call
        /// </summary>
        [TestFixtureSetUp]
        public void CreateDb()
        {
            Database.SetInitializer(new CreateDatabaseIfNotExists<GerritDb>());
            using (var ctx = new GerritDb())
            {
                ctx.Likes.Count();
            }
        }
        [SetUp]
        public void SetUp()
        {
            _tx = new TransactionScope(TransactionScopeOption.RequiresNew);
        }
        [TearDown]
        public void Revert()
        {
            _tx.Dispose();
        }
        [TestFixtureTearDown]
        public void DropDb()
        {
            Database.Delete("GerritDb");
        }
    }
}
