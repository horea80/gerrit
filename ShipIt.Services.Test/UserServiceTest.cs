﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using NUnit.Framework;

namespace ShipIt.Services.Test
{
    [TestFixture]
    public class UserServiceTest
    {
        [Test, Category("Integration")]
        public void GetUsersFromSlack()
        {
            var service = new UserService();
            IEnumerable<SlackUser> users = service.GetSlackUserList();
            Assert.Greater(users.Count(), 250, "No of slack users");
        }

        [Test, Category("Integration")]
        public void PostMessageToSlack()
        {
            var service = new UserService();
            var slackUser = new SlackUser
            {
                Name = "horea.hopartean"
            };
            var response = service.SendSlackMessage(slackUser, "Mihai Pantea", String.Empty);
            Assert.AreEqual("ok", response, "Slack POST response");
        }

        [Test]
        public void TestShortEmailInference()
        {
            var service = new UserService();
            var longEmail = "ovidiu.deac@ullink.com";
            Assert.AreEqual("odeac@ullink.com", service.InferShortEmail(longEmail));
        }
        [Test]
        public void TestSlackMessage()
        {
            var service = new UserService();
            string expectedMessage = "{\"text\" : \"Mihai Pantea likes your comment myCommentUrl. \", \"channel\" : \"@horea.hopartean\", \"link-names\" : \"1\" }";
            string actualMessage = service.CreateSlackMessage("horea.hopartean", "Mihai Pantea", "myCommentUrl", false);
            Assert.AreEqual(expectedMessage, actualMessage, "Message was not formatted correctly");
        }
        [Test]
        public void TestSlackMessageWithCalltoAction()
        {
            var service = new UserService();
            string callToAction = string.Format("Go to {0} in order to install our Gerrit Like Chrome extension",
                UserService.ChromeWebstoreUrl);
            string expectedMessage = "{\"text\" : \"Mihai Pantea likes your comment myCommentUrl. " + callToAction + 
                "\", \"channel\" : \"@horea.hopartean\", \"link-names\" : \"1\" }";
            string actualMessage = service.CreateSlackMessage("horea.hopartean", "Mihai Pantea", "myCommentUrl", true);
            Assert.AreEqual(expectedMessage, actualMessage, "Message was not formatted correctly");
        }
    }
}
