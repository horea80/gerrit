﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace ShipIt.Services
{
    public class CommentService
    {
        private const int MinLikesNumberForTop = 1;

        public IEnumerable<Like> GetAllLikes()
        {
            using (var db = new GerritDb())
            {
                return db.Likes.ToList();
            }
        }

        public IEnumerable<Comment> GetAllComments()
        {
            using (var db = new GerritDb())
            {
                return db.Comments.OrderByDescending(comment => comment.LikesCounter).ToList();
            }
        }

        public IEnumerable<Comment> GetAllFilteredByLikesNumber(int minLikesNumber = MinLikesNumberForTop)
        {
            using (var db = new GerritDb())
            {
                return db.Comments
                    .Where(comment => comment.LikesCounter >= minLikesNumber).OrderByDescending(comment => comment.LikesCounter).ToList();
            }
        }

        public IEnumerable<Comment> GetAllFilteredByLikesNumberAndQueryString(int minLikesNumber = MinLikesNumberForTop, string queryString = null)
        {
            if (queryString == null)
                return GetAllFilteredByLikesNumber(minLikesNumber);

            string normalizedQueryString = queryString.ToLower();
            return GetAllFilteredByLikesNumber(minLikesNumber).
                    Where(comment => comment.AuthorName.ToLower().Contains(normalizedQueryString)
                                     || comment.Content.ToLower().Contains(normalizedQueryString));
        }

        public IEnumerable<Comment> GetAllFilteredByAuthorName(string authorName, int minLikesNumber = MinLikesNumberForTop)
        {
            if (authorName == null)
                return GetAllFilteredByLikesNumber(minLikesNumber);

            return
                GetAllFilteredByLikesNumber(minLikesNumber)
                    .Where(comment => comment.AuthorName.ToLower().Contains(authorName.ToLower()));
        }

        public IEnumerable<Comment> GetAllFilteredByCommentContent(string commentContentQueryString, int minLikesNumber = MinLikesNumberForTop)
        {
            if (commentContentQueryString == null)
                return GetAllFilteredByLikesNumber(minLikesNumber);

            return
                GetAllFilteredByLikesNumber(minLikesNumber)
                    .Where(comment => comment.Content.ToLower().Contains(commentContentQueryString.ToLower()));
        }

        public IEnumerable<Comment> GetAllFilteredByLikesNumber(string likesNoQueryString, int minLikesNumber = MinLikesNumberForTop)
        {
            int likesNo;
            int.TryParse(likesNoQueryString, out likesNo);

            return
                GetAllFilteredByLikesNumber(minLikesNumber)
                    .Where(comment => comment.LikesCounter == likesNo);
        }
        
        public long Count()
        {
            using (var db = new GerritDb())
            {
                return db.Comments.LongCount();
            }
        }

        public int Like(Like like)
        {
            using (var db = new GerritDb())
            {

                like.Timestamp = DateTime.Now;
                db.Likes.Add(like);
                var comment = db.Comments.SingleOrDefault(c => c.CommentId == like.CommentId);
                if (comment == null)
                {
                    comment = new Comment
                    {
                        AuthorId = like.AuthorId,
                        CommentId = like.CommentId,
                        Url = like.Url,
                        Content = like.Content,
                        LikesCounter = 0,
                        CommentDate = like.CommentDate,
                        AuthorName = like.AuthorName
                    };
                    db.Comments.Add(comment);
                }
                comment.LikesCounter += 1;
                db.SaveChanges();
                return comment.LikesCounter;
            }
        }

        public int Unlike(string commentId, string fanId)
        {
            using (var db = new GerritDb())
            {
                var like = new Like {CommentId = commentId, FanId = fanId};
                db.Likes.Attach(like);
                db.Likes.Remove(like);
                var comment = db.Comments.SingleOrDefault(c => c.CommentId == commentId);
                if (comment.LikesCounter > 0)
                {
                    comment.LikesCounter--;
                }
                db.SaveChanges();
                return comment.LikesCounter;
            }
        }

        public Comment GetCommentById(string commentId)
        {
            using (var db = new GerritDb())
            {
                return db.Comments.SingleOrDefault(c => c.CommentId == commentId);
            }
        }

        public IEnumerable<Like> GetLikes(string commentId)
        {
            using (var db = new GerritDb())
            {
                return db.Likes.Where(l => l.CommentId == commentId).ToList();
            }
        }
    }
}
