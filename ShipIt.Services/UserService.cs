﻿
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using Newtonsoft.Json;

namespace ShipIt.Services
{
    public class UserService
    {
        internal static readonly string ChromeWebstoreUrl =
            @"https://chrome.google.com/webstore/detail/gerrit-like-ullink/fgmcipomldknehofpjkoiooidlokhjol/related?hl=en-US&gl=FR";
        public IEnumerable<SlackUser> GetSlackUserList()
        {
            string users = LoadUsersFromEmbeddedResource("ShipIt.Services.SlackUserList.txt");
            return ExtractSlackUsersFromJson(users);
        }

        public IEnumerable<SlackUser> GetSlackUserListFromServer()
        {
            return GetSlackUserListFromInternet();
        }

        private IEnumerable<SlackUser> GetSlackUserListFromInternet()
        {
            string url = "https://slack.com/api/users.list?token=xoxp-2152543373-2302538266-2481279186-940d1a";
            HttpWebRequest request = (HttpWebRequest) WebRequest.Create(url);
            HttpWebResponse response = (HttpWebResponse) request.GetResponse();

            using (Stream dataStream = response.GetResponseStream())
            {
                var reader = new StreamReader(dataStream);
                string userList = reader.ReadToEnd();
                return ExtractSlackUsersFromJson(userList);
            }
        }

        private  IEnumerable<SlackUser> ExtractSlackUsersFromJson(string userList)
        {
            var retVal = new List<SlackUser>();
            dynamic users = JsonConvert.DeserializeObject(userList);
            dynamic members = users["members"];
            for (int i = 0; i < members.Count; i++)
            {
                retVal.Add(new SlackUser
                {
                    Name = members[i].name,
                    Email = members[i].profile.email
                });
            }
            return retVal;
        }

        public string SendSlackMessage(SlackUser authorSlackUser, string fanName, string url)
        {
            var httpWebRequest = (HttpWebRequest) WebRequest.Create("https://hooks.slack.com/services/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");
            httpWebRequest.ContentType = "application/json";
            httpWebRequest.Method = "POST";
            string jsonRequest = CreateSlackMessage(authorSlackUser.Name, fanName, url, true);
            
            using (var streamWriter = new StreamWriter(httpWebRequest.GetRequestStream()))
            {
                streamWriter.Write(jsonRequest);
                streamWriter.Flush();
                streamWriter.Close();

                var httpResponse = (HttpWebResponse) httpWebRequest.GetResponse();
                using (var streamReader = new StreamReader(httpResponse.GetResponseStream()))
                {
                    return streamReader.ReadToEnd();
                }
            }
        }

        internal string CreateSlackMessage(string authorSlackUserName, string fanName, string commentUrl, bool includeCallToAction)
        {
            string jsonRequest =
                String.Format(
                    "{{\"text\" : \"{0} likes your comment {1}. {2}\", \"channel\" : \"@{3}\", \"link-names\" : \"1\" }}",
                    fanName,
                    commentUrl,
                    includeCallToAction
                        ? string.Format("Go to {0} in order to install our Gerrit Like Chrome extension",
                            ChromeWebstoreUrl)
                        : string.Empty, 
                    authorSlackUserName
                    );
            return jsonRequest;
        }

        public SlackUser FindSlackUser(string longEmail, IEnumerable<SlackUser> slackUsers)
        {
            var authorSlackUser = slackUsers.FirstOrDefault(s => s.Email == longEmail);
            if (authorSlackUser != null) return authorSlackUser;

            string shortEmail = InferShortEmail(longEmail);
            return slackUsers.FirstOrDefault(s => s.Email == shortEmail);
        }

        public string InferShortEmail(string longEmail)
        {
            var fragments = longEmail.Split('.');
            if (fragments.Length == 0) return longEmail;
            return longEmail[0] + longEmail.Substring(fragments[0].Length + 1);
        }


        private string LoadUsersFromEmbeddedResource(string fileUri)
        {
            using (var stream = Assembly.GetExecutingAssembly().GetManifestResourceStream(fileUri))
            {
                using (var reader = new StreamReader(stream))
                {
                    var embeddedContent = reader.ReadToEnd();
                    return embeddedContent;
                }
            }
        }
    }
}
