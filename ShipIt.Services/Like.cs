using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShipIt.Services
{   
    public class Like
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string CommentId { get; set; }

        [Column(Order = 1)]
        [StringLength(50)]
        public string AuthorId { get; set; }

        [Key]
        [Column(Order = 2)]
        [StringLength(50)]
        public string FanId { get; set; }

        [Column(Order = 3)]
        [StringLength(50)]
        public string FanName { get; set; }

        [Column(Order = 4)]
        [StringLength(50)]
        public string AuthorName { get; set; }

        [Column(Order = 5)]
        public DateTime Timestamp { get; set; }

        [Column(Order = 6)]
        public string Content { get; set; }

        [Column(Order = 7)]
        public string Url { get; set; }

        [Column(Order = 8)]
        [StringLength(50)]
        public string CommentDate { get; set; }
    }
}
