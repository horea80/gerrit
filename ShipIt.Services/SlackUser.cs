﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ShipIt.Services
{
    public class SlackUser
    {
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
