namespace ShipIt.Services
{
    using System.Data.Entity;

    public class GerritDb : DbContext
    {
        public GerritDb()
            : base("name=GerritDb")
        {
        }

        public virtual DbSet<Like> Likes { get; set; }

        public virtual DbSet<Comment> Comments { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
        }
    }
}
