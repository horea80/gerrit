﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace ShipIt.Services
{
    public class Comment
    {
        [Key]
        [Column(Order = 0)]
        [StringLength(50)]
        public string CommentId { get; set; }

        [Column(Order = 1)]
        public string Content { get; set; }

        [Column(Order = 2)]
        public string Url { get; set; }

        [Column(Order = 3)]
        public string AuthorId { get; set; }

        [Column(Order = 4)]
        public int LikesCounter { get; set; }

        [Column(Order = 5)]
        [StringLength(50)]
        public string CommentDate { get; set; }

        [Column(Order = 6)]
        [StringLength(50)]
        public string AuthorName { get; set; }
    }
}
